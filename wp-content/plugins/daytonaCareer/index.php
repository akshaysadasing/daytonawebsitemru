<?php

/*
Plugin Name: Daytona Careers
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Careers in Website
*/
 
function cptDaytonaCareer() {
 
    $labels = array(
        'name' => _x( 'Career', 'daytonaCareers' ),
        'singular_name' => _x( 'Career', 'daytonaCareers' ),
        'add_new' => _x( 'Add New', 'daytonaCareers' ),
        'add_new_item' => _x( 'Add New', 'daytonaCareers' ),
        'edit_item' => _x( 'Edit', 'daytonaCareers' ),
        'new_item' => _x( 'New', 'daytonaCareers' ),
        'view_item' => _x( 'View', 'daytonaCareers' ),
        'search_items' => _x( 'Search', 'daytonaCareers' ),
        'not_found' => _x( 'No data found.', 'daytonaCareers' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'daytonaCareers' ),
        'parent_item_colon' => _x( 'Career:', 'daytonaCareers' ),
        'menu_name' => _x( 'Career', 'daytonaCareers' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Career.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/daytonaCareer/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('daytonaCareers', $args);
}
 
add_action('init', 'cptDaytonaCareer');

?>