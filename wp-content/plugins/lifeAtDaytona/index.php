<?php

/*
Plugin Name: Life @ Daytona
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Image for Life at Daytona
*/
 
function lifeAtDaytona() {
 
    $labels = array(
        'name' => _x( 'Life @ Daytona', 'daytonaLife' ),
        'singular_name' => _x( 'Life @ Daytona', 'daytonaLife' ),
        'add_new' => _x( 'Add New', 'daytonaLife' ),
        'add_new_item' => _x( 'Add New', 'daytonaLife' ),
        'edit_item' => _x( 'Edit', 'daytonaLife' ),
        'new_item' => _x( 'New', 'daytonaLife' ),
        'view_item' => _x( 'View', 'daytonaLife' ),
        'search_items' => _x( 'Search', 'daytonaLife' ),
        'not_found' => _x( 'No data found.', 'daytonaLife' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'daytonaLife' ),
        'parent_item_colon' => _x( 'Life @ Daytona:', 'daytonaLife' ),
        'menu_name' => _x( 'Life @ Daytona', 'daytonaLife' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Life @ Daytona.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/lifeAtDaytona/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('daytonaLife', $args);
}
 
add_action('init', 'lifeAtDaytona');

?>