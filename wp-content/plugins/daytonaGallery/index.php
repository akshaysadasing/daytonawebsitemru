<?php

/*
Plugin Name: Daytona Gallery
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Gallery for Life at Daytona
*/
 
function cptDaytonaGallery() {
 
    $labels = array(
        'name' => _x( 'Daytona Gallery', 'daytonaGallery' ),
        'singular_name' => _x( 'Daytona Gallery', 'daytonaGallery' ),
        'add_new' => _x( 'Add Album', 'daytonaGallery' ),
        'add_new_item' => _x( 'Add Album', 'daytonaGallery' ),
        'edit_item' => _x( 'Edit', 'daytonaGallery' ),
        'new_item' => _x( 'New', 'daytonaGallery' ),
        'view_item' => _x( 'View', 'daytonaGallery' ),
        'search_items' => _x( 'Search', 'daytonaGallery' ),
        'not_found' => _x( 'No data found.', 'daytonaGallery' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'daytonaGallery' ),
        'parent_item_colon' => _x( 'Daytona Gallery:', 'daytonaGallery' ),
        'menu_name' => _x( 'Daytona Gallery', 'daytonaGallery' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Daytona Gallery.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/daytonaGallery/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('daytonaGallery', $args);
}
 
add_action('init', 'cptDaytonaGallery');

?>