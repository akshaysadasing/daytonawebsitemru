<?php

/*
Plugin Name: Employees Voice
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Employees Voice in Website
*/
 
function employeesVoice() {
 
    $labels = array(
        'name' => _x( 'Employees Voice', 'employees-voice' ),
        'singular_name' => _x( 'Employees Voice', 'employees-voice' ),
        'add_new' => _x( 'Add New', 'employees-voice' ),
        'add_new_item' => _x( 'Add New', 'employees-voice' ),
        'edit_item' => _x( 'Edit', 'employees-voice' ),
        'new_item' => _x( 'New', 'employees-voice' ),
        'view_item' => _x( 'View', 'employees-voice' ),
        'search_items' => _x( 'Search', 'employees-voice' ),
        'not_found' => _x( 'No data found.', 'employees-voice' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'employees-voice' ),
        'parent_item_colon' => _x( 'Employees Voice:', 'employees-voice' ),
        'menu_name' => _x( 'Employees Voice', 'employees-voice' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Employees Voice.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/welcomeSlider/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('employees-voice', $args);
}
 
add_action('init', 'employeesVoice');

?>