<?php

/*
Plugin Name: Daytona Services
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Services in Website
*/
 
function cptteams() {
 
    $labels = array(
        'name' => _x( 'teams', 'teams' ),
        'singular_name' => _x( 'teams', 'teams' ),
        'add_new' => _x( 'Add New', 'teams' ),
        'add_new_item' => _x( 'Add New', 'teams' ),
        'edit_item' => _x( 'Edit', 'teams' ),
        'new_item' => _x( 'New', 'teams' ),
        'view_item' => _x( 'View', 'teams' ),
        'search_items' => _x( 'Search', 'teams' ),
        'not_found' => _x( 'No data found.', 'teams' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'teams' ),
        'parent_item_colon' => _x( 'What We Do:', 'teams' ),
        'menu_name' => _x( 'What We Do', 'teams' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'teams.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/teams/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('teams', $args);
}
 
add_action('init', 'cptteams');

?>
