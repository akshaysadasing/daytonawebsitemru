<?php

/*
Plugin Name: Welcome Slider
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Sliders in Home Page
*/
 
function welcomeSlider() {
 
    $labels = array(
        'name' => _x( 'Welcome Slider', 'welcome-slider' ),
        'singular_name' => _x( 'Welcome Slider', 'welcome-slider' ),
        'add_new' => _x( 'Add New', 'welcome-slider' ),
        'add_new_item' => _x( 'Add New', 'welcome-slider' ),
        'edit_item' => _x( 'Edit', 'welcome-slider' ),
        'new_item' => _x( 'New', 'welcome-slider' ),
        'view_item' => _x( 'View', 'welcome-slider' ),
        'search_items' => _x( 'Search', 'welcome-slider' ),
        'not_found' => _x( 'No data found.', 'welcome-slider' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'welcome-slider' ),
        'parent_item_colon' => _x( 'Welcome Slider:', 'welcome-slider' ),
        'menu_name' => _x( 'Home Slider', 'welcome-slider' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'Welcome Slider.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/welcomeSlider/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('welcome-slider', $args);
}
 
add_action('init', 'welcomeSlider');

?>