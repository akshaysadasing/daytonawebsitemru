<?php

/*
Plugin Name: Daytona Services
Plugin URI: https://daytonasystems.co.in
Version: 1.0
Author: Daytona Systems
Author URI: https://daytonasystems.co.in
Description: Plugin to add Services in Website
*/
 
function cptWhatWeDo() {
 
    $labels = array(
        'name' => _x( 'What We Do', 'daytonaWhatWeDo' ),
        'singular_name' => _x( 'What We Do', 'daytonaWhatWeDo' ),
        'add_new' => _x( 'Add New', 'daytonaWhatWeDo' ),
        'add_new_item' => _x( 'Add New', 'daytonaWhatWeDo' ),
        'edit_item' => _x( 'Edit', 'daytonaWhatWeDo' ),
        'new_item' => _x( 'New', 'daytonaWhatWeDo' ),
        'view_item' => _x( 'View', 'daytonaWhatWeDo' ),
        'search_items' => _x( 'Search', 'daytonaWhatWeDo' ),
        'not_found' => _x( 'No data found.', 'daytonaWhatWeDo' ),
        'not_found_in_trash' => _x( 'No data found in trash.', 'daytonaWhatWeDo' ),
        'parent_item_colon' => _x( 'What We Do:', 'daytonaWhatWeDo' ),
        'menu_name' => _x( 'What We Do', 'daytonaWhatWeDo' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'What We Do.',
        'supports' => array('title','editor','media-uploads','thumbnail','custom-fields','page-attributes'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => site_url()."/wp-content/plugins/whatWeDo/icon.png",
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('daytonaWhatWeDo', $args);
}
 
add_action('init', 'cptWhatWeDo');

?>