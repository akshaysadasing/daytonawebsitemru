$(function(){

	// ================== Table of Content =====================
	//
	// 		1. Common Javascript
	//			1.0 Common Variables
	//			1.1 Loading Effect
	// 			1.2 Move to the Block
	//			1.3 Auto Scroll
	//			1.4 Menu Open / Close
	//			1.5 Sticky Header
	//			1.6 Go to Top
	//			1.7 Scroll Init
	//			1.8 Escape Press
	// 		2. Pages
	//			2.1 Home
	//				2.1.1 Landing Section
	//
	// ============================================================

	// =======================================
	//			1. Common Javascript
	// =======================================

		// ================================================================
		//			1.0 Common Varibles
		// ================================================================

		// ================================================================
		//			1.1 Loading Effect
		// ================================================================

			var startLoading = function()
	        {
	            $("#loaderProgress").css("width","0%");
	            $("#loader").show();
	            $("#loaderProgress").animate({
	                width: '100%'
	            }, 400);
	        }

	        var stopLoading = function()
	        {
	            $("#loader").hide();
	            $("#loaderProgress").animate({
	                width: '0%'
	            }, 200);            
	        }

	        $('#test').on('click', function()
	        {
	        	startLoading();setTimeout(function(){stopLoading();}, 4000);
	        });

		// ================================================================
		//			1.2 Move to the Block
		// ================================================================

			$('a[href^="#"]').on('click',function (e)
			{
				e.preventDefault();
				var target = this.hash;
				if(target)
				{
					var $target = $(target);
					$('html, body').stop().animate(
					{
						'scrollTop': $target.offset().top - 80
					}, 600);
				}
			});

		// ================================================================
		//			1.3 Auto Scroll
		// ================================================================

			if(location.hash)
			{
				var hash = location.hash;				
				window.scroll(0,0);
				setTimeout(function()
				{
					$("a[href='"+hash+"']").click();
				}, 200);				
			}

		// ================================================================
		//			1.4 Menu Open / Close
		// ================================================================

			var initialFullMenu = 0;
			$('.menuBtn').on('click', function()
			{
				if(initialFullMenu == 0)
				{
					openMenu();
				}
				else
				{
					closeMenu();
				}				
			});

			function openMenu()
			{
				$('body').css({ "overflow" : "hidden" });
				$('.menuBtn').addClass('open');
				$('nav').addClass('open');
				initialFullMenu = 1;
			}

			function closeMenu()
			{
				$('.menuBtn').removeClass('open');
				$('nav').removeClass('open');
				initialFullMenu = 0;
				$('body').css({ "overflow" : "auto" });
			}

		// ================================================================
		//			1.5 Sticky Header
		// ================================================================

			var stickyNav = function()
			{
			    var scrollTop = $(window).scrollTop();
				if(scrollTop > 0)
				{
					$('header').addClass('fixed');
					$('header.home').removeClass('homepage');
				} 
				else
				{
					$('header').removeClass('fixed');
					$('header.home').addClass('homepage');
				}
			};

		// ================================================================
		//			1.6 Go to Top
		// ================================================================

        	var showGoToTop = function()
	        {
	            var scrollTop = $(window).scrollTop();
	            if(scrollTop > 400)
	            {
	                $('#goToTop').fadeIn(200);
	            } 
	            else
	            {
	                $('#goToTop').fadeOut(200);
	            }
	        };

	    // ================================================================
		//			1.6 Scroll Init
		// ================================================================

			$(window).on("load scroll", function()
	        {
	        	stickyNav();
	            showGoToTop();
	        });

        // ================================================================
		//			1.8 Escape Press
		// ================================================================

			$(document).keyup(function(e)
			{
               if (e.keyCode == 27)
               {
               		closeMenu();
               }
        	});

	// =======================================
	//			2. Pages
	// =======================================

		// ================================================================
		//			2.1 Home
		// ================================================================

			// ================================================================
			//			2.1.1 Landing Section
			// ================================================================

				if($('#homeSlider').length)
				{
					$('#homeSlider').flexslider(
					{
					    animation: "fade",
					    controlsContainer: "#homeSlider",
					    animationLoop: true,
					    slideshow: true,
					    slideshowSpeed: 6000,
					    animationSpeed: 800,
					    pauseOnHover: true,
					    pauseOnAction: false,
					    touch: true,
					    controlNav: true,
					    touch: true,
					    move: 1,
					    directionNav: false,
					    useCSS: false,
					    keyboard: true,
					    multipleKeyboard: true
					});
				}

			// ================================================================
			//			2.1.2 Life at Daytona
			// ================================================================

				if($('#lifeSlider').length)
				{
					$('#lifeSlider').flexslider(
					{
					    animation: "slide",
					    controlsContainer: "#lifeSlider",
					    animationLoop: true,
					    slideshow: true,
					    slideshowSpeed: 6000,
					    animationSpeed: 800,
					    pauseOnHover: false,
					    pauseOnAction: false,
					    touch: true,
					    controlNav: true,
					    touch: true,
					    move: 1,
					    directionNav: false,
					    useCSS: false,
					    keyboard: false,
					    multipleKeyboard: true
					});
				}				

			// ================================================================
			//			2.1.3 Employees Slider
			// ================================================================

				if($('#employeesSlider').length)
				{
					$('#employeesSlider').flexslider(
					{
					    animation: "slide",
					    controlsContainer: "#employeesSlider",
					    animationLoop: true,
					    slideshow: true,
					    slideshowSpeed: 6000,
					    animationSpeed: 800,
					    pauseOnHover: false,
					    pauseOnAction: false,
					    touch: true,
					    controlNav: true,
					    touch: true,
					    move: 1,
					    directionNav: false,
					    useCSS: true,
					    keyboard: true,
					    multipleKeyboard: true
					});
				}

});