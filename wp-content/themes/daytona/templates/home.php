<?php
/*
Template Name: Home
Template Post Type: page
*/
?>
<?php get_header(); ?>

	<?php get_welcome_slider(); ?>

	<?php what_we_do(); ?>

	<?php featured_images(3); ?>

	<?php employees_voice(); ?>

	<?php home_careers(3); ?>

	<?php home_news_and_events(4); ?>

<?php get_footer();?>