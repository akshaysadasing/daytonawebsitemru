<?php
/*
Template Name: News - Single
Template Post Type: post
*/
get_header();?>

	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function()
		{
			FB.init({
			appId      : '2245617542320683',
			xfbml      : true,
			version    : 'v3.2'
			});
			FB.AppEvents.logPageView();
		};
		(function(d, s, id)
		{
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<section id="newsSingle">
				<div class="wrapper col">
					<div class="wid-1-1">
						<ul class="breadcrumb mar-0-a of-hid">
							<li class="f-left l-s-n avenir-light"><a title="News & Events" class="trans-all" href="<?php echo site_url().'/news-and-events/';?>">News & Events</a></li>
							<li class="f-left l-s-n avenir-light"><?php echo the_title(); ?></li>
						</ul>
					</div>
					<div class="title t-a-c">
						<h1 class="avenir-black color-b"><?php the_title(); ?></h1>
						<div class="dis-i-b">
							<p class="date avenir-book pos-rel"><?php echo get_the_date('F j, Y'); ?></p>
						</div>						
					</div>
					<div class="mainImage">
						<img class="wid-1-1 dis-b" src="<?php echo the_post_thumbnail_url();?>">
					</div>
					<div id="content">
						<?php the_content(); ?>
					</div>
					<ul class="social of-hid">
						<li class="l-s-n f-left">
							<a title="Share" class="fb-share-button" href="#">
								<img class="dis-b mar-0-a" alt="Facebook" src="<?php bloginfo('template_directory'); ?>/images/icons/post/facebook.svg">
							</a>
						</li>
						<li class="l-s-n f-left">
							<a title="Tweet" href="https://twitter.com/intent/tweet?url=<?php echo get_permalink();?>&button_hashtag=DaytonaSystemsIndia&text=<?php the_title(); ?>">
								<img class="dis-b mar-0-a" alt="Twitter" src="<?php bloginfo('template_directory'); ?>/images/icons/post/twitter.svg">
							</a>
						</li>
						<li class="l-s-n f-left">
							<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink();?>&title=<?php the_title(); ?>&summary=<?php the_title(); ?>&source=LinkedIn">
								<img class="dis-b mar-0-a" alt="LinkedIn" src="<?php bloginfo('template_directory'); ?>/images/icons/post/linkedin.svg">
							</a>
						</li>
					</ul>
					<div id="prevNext">
						<div class="of-hid">
							<?php if (strlen(get_previous_post()->post_title) > 0) : ?>
							<div class="one trans-all">
								<a title="<?php echo get_previous_post()->post_title; ?>" href="<?php echo get_post_permalink(get_previous_post()->ID); ?>">
									<div class="inner">
										<h2 class="avenir-black color-p">Prev:</h2>
										<p class="avenir-heavy date"><?php echo date("F j, Y", strtotime(get_previous_post()->post_date)); ?></p>
										<h4 class="avenir-black color-b"><?php echo get_previous_post()->post_title; ?></h4>
										<p class="avenir-light desc">
											<?php echo substr(wp_strip_all_tags(get_post(get_previous_post()->ID)->post_content, true), 0, 220); ?>
										</p>
										<div class="read-more"><p class="avenir-black color-p">READ MORE</p></div>
									</div>
								</a>
							</div>
							<?php endif; ?>
							<?php if (strlen(get_next_post()->post_title) > 0) : ?>
							<div class="one trans-all">
								<a title="<?php echo get_next_post()->post_title; ?>" href="<?php echo get_post_permalink(get_next_post()->ID); ?>">
									<div class="inner">
										<h2 class="avenir-black color-p">Next:</h2>
										<p class="avenir-heavy date"><?php echo date("F j, Y", strtotime(get_next_post()->post_date)); ?></p>
										<h4 class="avenir-black color-b"><?php echo get_next_post()->post_title; ?></h4>
										<p class="avenir-light desc">
											<?php echo substr(wp_strip_all_tags(get_post(get_next_post()->ID)->post_content, true), 0, 220); ?>
										</p>
										<div class="read-more"><p class="avenir-black color-p">READ MORE</p></div>
									</div>
								</a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>
			<script type="text/javascript">
				$(document).ready(function()
				{
					$('.fb-share-button').on('click', function()
					{
						share();
					});				
				});
				function share()
				{
					FB.ui({
						method: 'share',
						title: '<?php the_title(); ?>',
						picture: '<?php echo the_post_thumbnail_url();?>',
						href: '<?php echo get_permalink();?>',
						quote: '<?php the_title(); ?>',
					}, function(response)
					{
						if(response && !response.error_code)
						{
							$("#notification p").html('Thank you for sharing the blog in Facebook. Keep visiting the site for more.<br>Have a nice day!');
							$("#notification").fadeIn(200);
						}
						else
						{
							$("#notification p").html('Facebook share could not be successful at this moment. Please try again later.<br>Thank you.');
							$("#notification").fadeIn(200);
						}
					});
				}
			</script>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer();?>
