<?php
/*
Template Name: Single Post
Template Post Type: page
*/
get_header();?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<section id="newsSingle">
				<div class="wrapper col">
					<div class="wid-1-1">
						<ul class="breadcrumb mar-0-a of-hid">
							<li class="f-left l-s-n avenir-light"><a title="Home" class="trans-all" href="<?php echo site_url();?>">Home</a></li>
							<li class="f-left l-s-n avenir-light"><?php echo the_title(); ?></li>
						</ul>
					</div>
					<div class="title t-a-c">
						<h1 class="avenir-black color-b"><?php the_title(); ?></h1>
						<div class="dis-i-b">
							<p class="date avenir-book pos-rel"><?php echo get_the_date('F, Y'); ?></p>
						</div>						
					</div>
					<div id="content">
						<?php the_content(); ?>
					</div>
				</div>
			</section>
		<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer();?>
