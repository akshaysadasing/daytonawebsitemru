<?php
/*
Template Name: Life at Daytona
Template Post Type: page
*/
?>
<?php get_header(); ?>

	<?php featured_images(3); ?>

	<?php employees_voice(); ?>

	<?php latest_gallery(3); ?>

<?php get_footer();?>