<?php
/*
Template Name: Teams
Template Post Type: page, post
*/
?>
<?php get_header(); ?>
<section>
    <div>
        <h2 class="din-black color-p" style="text-align: center">Mauritius Teams</h2>
        <br/>
        <?php echo do_shortcode( '[TEAM_B id=147]');?>

    </div>
</section>
<?php get_footer();?>
