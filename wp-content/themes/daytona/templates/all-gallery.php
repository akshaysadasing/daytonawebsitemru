<?php
/*
Template Name: All Gallery
Template Post Type: page
*/
?>
<?php get_header(); ?>

	<?php gallery_heading(); ?>

	<?php gallery(); ?>

<?php get_footer();?>