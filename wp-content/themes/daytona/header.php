<!DOCTYPE HTML>
<html>
	<head>
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="copyright" content="" />
		<meta name="description" content="Daytona Systems India specializes in SaaS applications for FinTechs, Telecos and eCommerce. We are technology partners for some of the most innovative startups." />
		<meta name="keywords" content="daytona, fintech, finance, ecommerce, bangalore, startup, remittance, financial companies, banking product, south africa, mauritius, uk, financial solutions, consumer needs, technological solutions" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="language" content="English">
		<!--
		<meta http-equiv="Content-Security-Policy" content="img-src https://*;">
		-->		
		<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/96x96.png?<?php echo strtotime("now"); ?>">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/images/favicon/48x48.png?<?php echo strtotime("now"); ?>">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/images/favicon/32x32.png?<?php echo strtotime("now"); ?>">       
        <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/images/favicon/16x16.png?<?php echo strtotime("now"); ?>">
        <!--
        <link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/fonts.css' type='text/css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/base.css' type='text/css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/pages.css' type='text/css' type='text/css' media='all'/>
		<link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/responsive.css' type='text/css' type='text/css' media='all'/>
		-->
		<!--
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.flexslider-min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/particles.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/base.js"></script>
		-->
		
		<link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/main.min.css?<?php echo strtotime("now"); ?>' type='text/css' media='all'/>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/main.min.js?<?php echo strtotime("now"); ?>"></script>
		
		<?php wp_head(); ?>
	</head>
	<body id="top">

		<?php loader(); ?>

		<div id="loader"><div id="loaderProgress"></div></div>

		<div class="menuBtn cur-pointer">
			<span class="trans-all"></span>
			<span class="trans-all"></span>
			<span class="trans-all"></span>
		</div>

		<a href="#top">
			<div id="goToTop" class="t-a-c b-r-100">
				<img alt="Top" class="mar-0-a wid-a dis-b" src="<?php bloginfo('template_directory'); ?>/images/icons/common/top.svg">
			</div>
		</a>

		<?php if(is_front_page()){ ?>
		<header class="trans-all wid-1-1 home homepage">
		<?php } else { ?>
		<header class="trans-all wid-1-1">
		<?php } ?>
			<div class="mar-0-a of-hid">
				<div class="f-left logo">
					<a title="Daytona Systems" href="<?php echo get_site_url(); ?>">
						<img alt="Daytona" src="<?php bloginfo('template_directory'); ?>/images/logo/logo.svg">
					</a>
				</div>
				<div class="f-left menu-holder">
					<nav>
						<?php wp_nav_menu(array('theme_location'=>'primary')); ?>
					</nav>
				</div>
			</div>
		</header>
