module.exports = function(grunt)
{
    grunt.initConfig(
    {
        pkg: grunt.file.readJSON('package.json'),
        uglify: 
        {
            my_target:
            {
                files:
                {
                    '../js/main.min.js':
                    [
                        '../js/jquery-3.2.1.min.js',
                        '../js/jquery.flexslider-min.js',
                        '../js/particles.js',
                        '../js/base.js'
                    ]
                }
            }
        },
        cssmin:
        {
            options:
            {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: 
            {
                files: 
                {
                    '../css/main.min.css': 
                    [
                        '../css/fonts.css',
                        '../css/base.css',
                        '../css/pages.css',
                        '../css/responsive.css'
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Run 'grunt default'

    grunt.registerTask('default', [ 'uglify', 'cssmin' ]);
};