<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage DaytonaSystems
 * @since DaytonaSystems 1.0
 */

get_header(); ?>

	<section id="error404">
		<div class="holder t-a-c">
			<h1 class="din-black color-p">ERROR 404</h1>
			<h4 class="din-black color-b">The page you are looking for has vanised or does not exist.</h4>
			<hr>
			<div class="t-a-c">
				<div class="dis-i-b">
					<a href="<?php echo site_url().'/';?>">
						<button class="color-p avenir-black t-t-u trans-all">Go to main page</button>
					</a>
				</div>
			</div>			
		</div>
	</section>

<?php get_footer(); ?>
