		
		<div id="findUs">
			<div class="holder of-hid pos-rel of-vis">
				<div class="wrapper">
					<div class="title">
						<h1 class="din-black color-p">Our <br>Hideout</h1>
					</div>
				</div>
				<div class="g-map" id="google-map">
				</div>
				<div class="info">
					<h4 class="din-black color-b">Contact us</h4>
					<ul>
						<li class="l-s-n of-hid">
							<div class="f-left img">
								<a title="Location" href="#" target="_blank">
									<img alt="Location" src="<?php bloginfo('template_directory'); ?>/images/icons/contact/location.svg">
								</a>
							</div>
							<div class="def">
								<p class="avenir-light"><a href="https://www.google.com/maps/place/Daytona+Systems+(Indiahttps://www.google.com/maps/dir/-20.0586376,57.5646011/-20.0222563,57.5799572/@-20.0459657,57.5567615,14z/data=!3m1!4b1!4m4!4m3!1m1!4e1!1m0" target="_blank">Suite C1-201, Level 2, Block C, <br> Grand Baie La Croisette 30517 Grand Baie, Mauritius</a></p>
							</div>
						</li>
						<li class="l-s-n of-hid">
							<div class="f-left img">
								<a title="Phone" href="tel:+2302696239" target="_blank">
									<img alt="Phone" src="<?php bloginfo('template_directory'); ?>/images/icons/contact/phone.svg">
								</a>
							</div>
							<div class="def">
								<p class="avenir-light"><a href="tel:+2302696239"  target="_blank">+2302696239</a></p>
							</div>
						</li>
						<li class="l-s-n of-hid">
							<div class="f-left img">
								<a href="mailto:info@daytonasystems.co.in">
									<img alt="" src="<?php bloginfo('template_directory'); ?>/images/icons/contact/email.svg">
								</a>
							</div>
							<div class="def">
								<p class="avenir-light"><a href="mailto:hr@daytonatec.com">
                                        hr@daytonatec.com</a></p>
							</div>
						</li>
					</ul>
					<h4 class="din-black color-b">Social</h4>
					<ul>
						<li class="l-s-n of-hid">
							<div class="f-left img">
								<a title="Facebook" href="https://www.facebook.com/daytonatec" target="_blank">
									<img alt="Facebook" src="<?php bloginfo('template_directory'); ?>/images/icons/social/facebook.svg">
								</a>
							</div>
							<div class="def">
								<p class="avenir-light">
									<a title="Facebook" href="https://www.facebook.com/daytonatec" target="_blank">https://www.facebook.com/daytonatec</a>
								</p>
							</div>
						</li>
						<li class="l-s-n of-hid">
							<div class="f-left img">
								<a title="LinkedIn" href="https://www.linkedin.com/company/daytona-systems-india/" target="_blank">
									<img alt="LinkedIn" src="<?php bloginfo('template_directory'); ?>/images/icons/social/linkedin.svg">
								</a>
							</div>
							<div class="def">
								<p class="avenir-light">
									<a title="LinkedIn" href="https://www.linkedin.com/company/daytona-technologies/" target="_blank">https://www.linkedin.com/company/daytona-technologies/</a>
								</p>
							</div>
						</li>
<!--						<li class="l-s-n of-hid">-->
<!--							<div class="f-left img">-->
<!--								<a title="Twitter" href="https://twitter.com/daytonaindia" target="_blank">-->
<!--									<img alt="Twitter" src="--><?php //bloginfo('template_directory'); ?><!--/images/icons/social/twitter.svg">-->
<!--								</a>-->
<!--							</div>-->
<!--							<div class="def">-->
<!--								<p class="avenir-light">-->
<!--									<a title="Twitter" href="https://twitter.com/daytonaindia" target="_blank">https://twitter.com/daytonaindia/</a>-->
<!--								</p>-->
<!--							</div>-->
<!--						</li>-->
					</ul>
					<h4 class="din-black color-b">Join us</h4>
					<ul>
						<li class="l-s-n">
							<p class="avenir-light">Got what it takes? We’d love to hear from you.<br>You may write to us at <a href="mailto:hr@daytonatec.com"><span class="color-p">hr@daytonatec.com</span></a></p>
						</li>
					</ul>
					<div class="btn">
						<div class="inner">
							<div class="dis-i-b">
								<a href="<?php echo get_site_url().'/careers/'; ?>">
									<button id="test" class="color-p avenir-black t-t-u trans-all">Job Openings</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="sayHello">
			<div class="holder">
				<div class="wrapper">
					<div class="title">
						<h1 class="din-black color-white">Say hi,<br>we're waving</h1>
					</div>
					<div class="formholder">
						<?php
							echo do_shortcode(
								'[contact-form-7 id="77" title="Footer Contact Form"]'
							);
						?>
					</div>
				</div>
			</div>
		</div>

		<footer>
			<div class="holder">
				<div class="top">
					<div class="wrapper">
						<div class="wid-1-1 of-hid">
							<div class="f-left wid-1-4">
								<h4 class="din-black">Company</h4>
								<ul>
									<li class="l-s-n avenir-light"><a title="Home" class="trans-all" href="<?php echo get_site_url(); ?>">Home</a></li>
									<li class="l-s-n avenir-light"><a title="About" class="trans-all" href="<?php echo get_site_url().'/about-us/'; ?>">About</a></li>
									<li class="l-s-n avenir-light"><a title="Life at Daytona" class="trans-all" href="<?php echo get_site_url().'/life-at-daytona/'; ?>">Life at Daytona</a></li>
									<li class="l-s-n avenir-light"><a title="Find us" class="trans-all" href="#findUs">Find us</a></li>
									<li class="l-s-n avenir-light"><a title="Privacy Policy" class="trans-all" href="<?php echo get_site_url().'/privacy-policy/'; ?>">Privacy Policy</a></li>
								</ul>
							</div>
							<div class="f-left wid-1-4">
								<h4 class="din-black">Latest</h4>
								<ul>
									<li class="l-s-n avenir-light"><a title="News & Events" class="trans-all" href="<?php echo get_site_url().'/news-and-events/'; ?>">News & Events</a></li>
									<li class="l-s-n avenir-light"><a title="Careers" class="trans-all" href="<?php echo get_site_url().'/careers/'; ?>">Careers</a></li>
									<li class="l-s-n avenir-light"><a title="Gallery" class="trans-all" href="<?php echo get_site_url().'/gallery/'; ?>">Gallery</a></li>
								</ul>
							</div>
							<div class="f-left wid-1-4">
								<h4 class="din-black">Contact</h4>
								<ul>
									<li class="l-s-n avenir-light"><a title="Location" class="trans-all" target="_blank" href="https://www.google.com/maps/dir/?api=1&destination=-20.02225631787%2C57.57995724678&fbclid=IwAR0lUEVlY43QNgiP-Fx5YeOlVIgvqj4867MRuPJxm0NUkzgYRM4tUdUFElw">Location</a></li>
									<li class="l-s-n avenir-light"><a title="Phone" class="trans-all" target="_blank" href="tel:+2302696239">Phone</a></li>
									<li class="l-s-n avenir-light"><a title="Email" class="trans-all" href="mailto:hr@daytonatec.com">Email</a></li>
								</ul>
							</div>
							<div class="f-left wid-1-4">
								<h4 class="din-black">Social</h4>
								<ul>
									<li class="l-s-n avenir-light"><a title="Facebook" class="trans-all" href="https://www.facebook.com/daytonatec" target="_blank">Facebook</a></li>
									<li class="l-s-n avenir-light"><a title="Linkedin" class="trans-all" href="https://www.linkedin.com/company/daytona-technologies/" target="_blank">LinkedIn</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom t-a-c">
					<div class="dis-i-b">
						<img src="<?php bloginfo('template_directory'); ?>/images/logo/logo-white.svg">
						<p class="avenir-light">Copyright © <?php echo date('Y'); ?></p>
						<p class="avenir-light">Daytona Technologies Ltd.</p>
						<p class="avenir-light">All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</footer>

		<script src="https://maps.googleapis.com/maps/api/js?key=6LfK05YUAAAAAG5nY4i3-9JAEwvykTvst-BxZrf6"></script>
		<script type="text/javascript">
			window.marker = null;		 
			function initialize() {
				var map;
				var latlng_pos = new google.maps.LatLng(12.916150,77.651593);
				var style = [ 
					{ "featureType": "road", 
					   "elementType": 
					   "labels.icon", 
					   "stylers": [ 
						  { "saturation": 1 }, 
						  { "gamma": 1 }, 
						  { "visibility": "on" }, 
						  { "hue": "#e6ff00" } 
					   ] 
					},
					{ "elementType": "geometry", "stylers": [ 
						{ "saturation": -100 } 
					  ] 
					} 
				];
				var mapOptions = {
					center: latlng_pos,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					zoom: 12,
					backgroundColor:"#f0f0f0",
					panControl:false,
					zoomControl:true,
					mapTypeControl:false,
					scaleControl:false,
					streetViewControl:false,
					overviewMapControl:false,
					zoomControlOptions: {
						style:google.maps.ZoomControlStyle.SMALL
					}
				}
				map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
				var mapType = new google.maps.StyledMapType(style, { name: "Grayscale" });
				map.mapTypes.set('grey', mapType);
				map.setMapTypeId('grey');
				var locations = [
					['Daytona Technologies -  Mauritius<br>Suite C1-201, Level 2<br> Block C Grand Baie La Croisette<br> Grand Bay<br> Mauritius', -20.021798, 57.579111]
				];
			    var marker, i;
			    var bounds = new google.maps.LatLngBounds();
			    var markerImage = '<?php bloginfo('template_directory'); ?>/images/icons/common/marker.svg';
				var pinIcon = new google.maps.MarkerImage(markerImage,null,null, null,new google.maps.Size(28, 42));
				var infowindow = new google.maps.InfoWindow();
				for (i = 0; i < locations.length; i++)
				{  
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map,
						icon: pinIcon,
					});
					bounds.extend(marker.position);

					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				map.fitBounds(bounds);
				var listener = google.maps.event.addListener(map, "idle", function ()
				{
				    google.maps.event.removeListener(listener);
				});
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>

		<?php if(is_front_page()) { ?>
			<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/app.js"></script>
		<?php } ?>

		<?php wp_footer();?>
		
	</body>
</html>
