<?php 

	// ================== Table of Content =====================
	//
	//		0. SetUp
	//			0.1 Add SVG capabilities
	// 			0.2 Enqueue Scripts
	//			0.3 Theme Setup
	// 			0.4 Custom Title Addition
	// 			0.5 Allow SVG Upload
	//			0.6 Disallow Author Based Search
	//			0.7 Loader
	//
	// ============================================================

	// =======================================
	//			0. SetUp
	// =======================================

		// =======================================
		//			0.1. Add SVG capabilities
		// =======================================

			function wpcontent_svg_mime_type($mimes=array())
			{
				$mimes['svg']  = 'image/svg+xml';
				$mimes['svgz'] = 'image/svg+xml';
				return $mimes;
			}
			add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );

		// =======================================
		//			0.2 Enqueue Scripts
		// =======================================

			add_filter('wp_enqueue_scripts','ds_blog_script_enqueue' ,0);

			function ds_blog_script_enqueue()
			{
				wp_enqueue_script('customjs', get_template_directory_uri().'/js/loader.js', array(), strtotime("now"), true );
				wp_localize_script('customjs', 'DSAjax', array(
					'ajaxurl' => admin_url('admin-ajax.php'),
					'check_nonce' => wp_create_nonce('ds-special-string'),
					'security' => wp_create_nonce('my-special-string')
				));				
			}			

		// =======================================
		//			0.3 Theme Setup
		// =======================================	

			function daytona_blog_theme_setup()
			{
				add_theme_support('menus');
				register_nav_menu('primary','Header Navigations');
			}
			add_action('init', 'daytona_blog_theme_setup');
			add_theme_support('post-thumbnails');

		// =======================================
		//			0.4 Custom Title Addition
		// =======================================

			function add_page_title($title)
			{
			    if (!is_single())
			        return $title;
			    global $wp_query;
			    if (isset($wp_query->post->post_title))
			    {
			        return $wp_query->post->post_title;
			    }
			    return $title;
			}
			add_filter('wp_title', 'add_page_title');

		// =======================================
		//			0.5 Allow SVG Upload
		// =======================================

			function cc_mime_types($mimes)
			{
				$mimes['svg'] = 'image/svg+xml';
				$mimes['svgz'] = 'image/svg+xml';
				return $mimes;
			}
			add_filter('upload_mimes', 'cc_mime_types');

			function add_file_types_to_uploads($file_types)
			{
				$new_filetypes = array();
				$new_filetypes['svg'] = 'image/svg+xml';
				$file_types = array_merge($file_types, $new_filetypes);
				return $file_types;
			}
			add_action('upload_mimes', 'add_file_types_to_uploads');

		// =======================================
		//			0.6 Disallow Author Based Search
		// =======================================

			function authorQueryRedirect()
			{
			    if(is_author())
			    {
			        wp_redirect(home_url());
			    }
			}
			add_action('template_redirect', 'authorQueryRedirect');

		// =======================================
		//			0.7 Loader
		// =======================================

			function loader()
			{
				echo '<div id="loaderMain">
					<div class="loadingBox">
						<ul><li></li><li></li></ul>
					</div>
				</div>';
			}

	// =======================================
	//			1. Home Page
	// =======================================

		// =======================================
		//			1.1 Welcome Slider
		// =======================================		

			function get_welcome_slider()
			{
				$welcomeSliders = new WP_Query(
					array(
						'post_type'			=> 'welcome-slider', 
						'post_status'		=> 'publish', 
						'order_by'			=> 'order', 
						'order'				=> 'asc',
					)
				);
				if ($welcomeSliders->have_posts()):
					echo '<div id="homeLanding"><div class="height pos-rel cover-img" style="background-image: url('.get_template_directory_uri().'/images/home/landing/coding-background.jpg);"><div id="particles-js"></div><div class="holder mar-0-a"><div id="homeSlider" class="t-a-l pos-rel flesxlider"><ul class="slides">';
							while($welcomeSliders->have_posts()):$welcomeSliders->the_post();
								echo '<li>';
									echo '<div class="of-hid">';
										echo '<div class="icon pos-rel f-left">';
											echo '<img src="';
												echo the_post_thumbnail_url();
											echo '">';
										echo '</div>';
										echo '<div class="desc f-left">';
											echo '<h4 class="din-black color-p">';
												the_title();
											echo '</h4>';
											echo '<p class="avenir-light">';
												echo wp_strip_all_tags(get_the_content());
											echo '</p>';
										echo '</div>';
									echo '</div>';
								echo '</li>';
							endwhile;
					echo '</ul></div></div><a href="#whatWeDo"><div class="scroll"><div class="wheel b-r-100 wheelscroll"></div></div></a></div></div>';
					wp_reset_postdata();
				else :
				endif;
			}

		// =======================================
		//			1.2 What we do
		// =======================================

			function what_we_do()
			{
				echo '<div id="whatWeDo">';
					echo '<div class="holder pos-rel cover-img" style="background-image: url('.get_template_directory_uri().'/images/home/what-we-do/bg.jpg);">';
						echo '<div class="v-cover">';
							echo '<div class="v-one">';
								echo '<div class="wrapper">';
									echo '<h1 class="din-black color-white">We provide R ’n’ D <br>related to Info-Tech Bespoke Sofwares & Solutions.</h1>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}

		// =======================================
		//			1.3 Life at Daytona - Featured
		// =======================================

			function featured_images($limit)
			{
				echo '<div id="lifeAtDaytona"><div class="holder pos-rel"><div class="wrapper of-hid">';
				$featuredImages = new WP_Query(
					array(
						'post_type'			=> 'daytonalife', 
						'post_status'		=> 'publish', 
						'order_by'			=> 'publish_date', 
						'order'				=> 'desc',
						'posts_per_page'	=> $limit
					)
				);
				if ($featuredImages->have_posts()):
					echo '<div class="holder f-left pos-rel of-vis img"><div class="v-cover"><div class="v-one"><div class="lifeSliderHolder pos-rel"><div id="lifeSlider" class="flexslider"><ul class="slides">';
						while($featuredImages->have_posts()):$featuredImages->the_post();
							echo '<li>';
								echo '<div class="image cover-img" style="background-image: url(';
									echo the_post_thumbnail_url();
								echo ');">';
								echo '</div>';
							echo '</li>';
						endwhile;
					echo '</ul></div></div></div></div></div>';
					wp_reset_postdata();
				else :
				endif;
				echo '<div class="holder f-left pos-rel of-vis desc"><div class="v-cover"><div class="v-one"><div class="description">
						<h2 class="din-black color-b">Life @<br>Daytona</h2>
						<p class="avenir-light">Daytona Technologies is a small group of passionate employees who share a passion for programming, thrashing out design around the whiteboard or working out the product vision and plan. We firmly believe in always doing what’s best for employees in our company.</p>';
					echo '</div></div></div></div></div>';
					echo '<img class="bg-img" src="'.get_template_directory_uri().'/images/home/life-at-daytona/bg.jpg">';
				echo '</div></div>';
			}

		// =======================================
		//			1.4 Employees Voice
		// =======================================

			function employees_voice()
			{
				$employeesVoice = new WP_Query(
					array(
						'post_type'			=> 'employees-voice', 
						'post_status'		=> 'publish', 
						'order_by'			=> 'publish_date', 
						'order'				=> 'desc'
					)
				);
				if ($employeesVoice->have_posts()):
					echo '<div id="happyEmployees"><div class="holder pos-rel cover-img" style="background-image: url('.get_template_directory_uri().'/images/common/lines-background.svg);"><div class="v-cover"><div class="v-one"><div class="employeesSliderHolder pos-rel mar-0-a"><div id="employeesSlider" class="flexslider"><ul class="slides">';
						while($employeesVoice->have_posts()):$employeesVoice->the_post();
							echo '<li>';
								echo '<div class="p-wrap t-a-c pos-rel">';
									echo '<p class="avenir-light mar-0-a">“ ';
										echo wp_strip_all_tags(get_the_content());
									echo ' ”</p>';
								echo '</div>';
								echo '<div class="info mar-0-a of-hid">
										<div class="f-left img cover-img b-r-100" style="background-image: url(';
											echo the_post_thumbnail_url();
											echo ');">';
										echo '</div>';
										echo '<div class="f-left desc">';
											echo '<p class="avenir-heavy">';
												echo the_title();
											echo '</p>';
											echo '<p><span class="avenir-book">';
												echo get_post_meta( get_the_ID(), 'position', true );
											echo '</span></p>';
										echo '</div>';
									echo '</div>';
								echo '</li>';
						endwhile;
					echo '</ul></div></div></div></div></div></div>';
					wp_reset_postdata();
				else :
				endif;
			}

		// =======================================
		//			1.5 Careers
		// =======================================

			function home_careers($limit)
			{
				echo '<div id="careers">
						<div class="holder of-hid pos-rel">
							<img class="image" src="'.get_template_directory_uri().'/images/home/careers/bg.jpg">
							<div class="wrapper">
								<div class="title">
									<h1 class="din-black color-p">Career <br>Opportunities</h1>
									<p class="avenir-light">Daytona Technologies Ltd is part of the award-winning international organisation who focuses  <a title="Learn more" href="'.get_site_url().'/careers/'.'"><span class="color-p"> learn more ...</span></a></p>
								</div>';
								$careers = new WP_Query(
									array(
										'post_type'			=> 'daytonacareers', 
										'post_status'		=> 'publish', 
										'orderby'			=> 'menu_order', 
										'order'				=> 'asc',
										'posts_per_page'	=> $limit
									)
								);
								if ($careers->have_posts()):
									echo '<div class="openings">';
										while($careers->have_posts()):$careers->the_post();
											echo '<div class="one trans-all">
												<a title="';
													the_title();
												echo '"href="';
												the_permalink();
												echo '">
													<div class="post of-hid">
														<div class="f-left icon">';
															echo '<img alt="';
																the_title();
															echo '" src="';
																echo the_post_thumbnail_url();
															echo '"/>';
														echo '</div>
														<div class="f-left desc">
															<h4 class="din-black color-b">';
																the_title();
															echo '</h4>
															<p class="avenir-light defn">';
																echo substr(strip_tags(wp_strip_all_tags(get_the_content(), true)), 0, 98);
															echo ' ….</p>
															<div class="read-more">
																<p class="avenir-black color-p">READ MORE</p>
															</div>
														</div>
													</div>
												</a>';
										echo '</div>';
										endwhile;
									echo '</div>';
									wp_reset_postdata();
								else :
								endif;
						echo '<div class="t-a-c">
								<div class="dis-i-b">
									<a title="View all" href="'.get_site_url().'/careers/'.'">
										<button class="color-p avenir-black t-t-u trans-all">View all</button>
									</a>
								</div>
							</div>';
						echo '</div>
						</div>
					</div>';
			}

		// =======================================
		//			1.6 News & Events
		// =======================================

			function home_news_and_events($limit)
			{
				echo '<div id="news">
						<div class="holder of-hid pos-rel">
							<img class="image" src="'.get_template_directory_uri().'/images/common/lines-background.svg">
							<div class="wrapper">
								<div class="title">
									<h1 class="din-black color-p">News <br>& Events</h1>
								</div>';
								$newsAndEvents = new WP_Query(
									array(
										'post_type'			=> 'post', 
										'post_status'		=> 'publish', 
										'order_by'			=> 'publish_date', 
										'order'				=> 'desc',
										'posts_per_page'	=> $limit
									)
								);
								if ($newsAndEvents->have_posts()):
									echo '<div class="allHolder mar-0-a of-hid"><div class="all">';
										while($newsAndEvents->have_posts()):$newsAndEvents->the_post();
											echo '<div class="one f-left">';
												echo '<a title="';
														the_title();
													echo '"href="';
													the_permalink();
													echo '">';
														echo '<div class="inner trans-all">';
															echo '<p class="avenir-black pos-rel date">';
																echo get_the_date('F j, Y');
															echo '</p>';
															echo '<h4 class="avenir-black color-b">';
																the_title();
															echo '</h4>';
															echo '<p class="avenir-light con">';
																echo substr(strip_tags(wp_strip_all_tags(get_the_content(), true)), 0, 148);
															echo ' ….</p>';
															echo '<div class="read-more">';
																echo '<p class="avenir-black color-p">READ MORE</p>';
															echo '</div>';
														echo '</div>';
													echo '</a>';
											echo '</div>';
										endwhile;
									echo '</div></div>';
									wp_reset_postdata();
								else :
								endif;
						echo '<div class="t-a-c">
								<div class="dis-i-b">
									<a title="View all" href="'.get_site_url().'/news-and-events/'.'">
										<button class="color-p avenir-black t-t-u trans-all">View all</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}

	// =======================================
	//			2. News & Events
	// =======================================

		function news_and_events()
		{
			echo '<div id="news">
					<div class="holder of-hid pos-rel">
						<img class="image" src="'.get_template_directory_uri().'/images/common/lines-background.svg">
						<div class="wrapper">
							<div class="title">
								<h1 class="din-black color-p">News <br>& Events</h1>
							</div>';
							$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$newsAndEvents = new WP_Query(
								array(
									'post_type'			=> 'post', 
									'post_status'		=> 'publish', 
									'posts_per_page'	=> 6, 
									'order_by'			=> 'post_date', 
									'order'				=> 'desc',
									'paged'				=> $paged
								)
							);
							if ($newsAndEvents->have_posts()):
								echo '<div class="allHolder mar-0-a of-hid"><div class="all">';
									while($newsAndEvents->have_posts()):$newsAndEvents->the_post();
										echo '<div class="one f-left">';
											echo '<a title="';
													the_title();
												echo '"href="';
												the_permalink();
												echo '">';
													echo '<div class="inner trans-all">';
														echo '<p class="avenir-black pos-rel date">';
															echo get_the_date('F j, Y');
														echo '</p>';
														echo '<h4 class="avenir-black color-b">';
															the_title();
														echo '</h4>';
														echo '<p class="avenir-light con">';
															echo substr(strip_tags(wp_strip_all_tags(get_the_content(), true)), 0, 148);
														echo ' ….</p>';
														echo '<div class="read-more">';
															echo '<p class="avenir-black color-p">READ MORE</p>';
														echo '</div>';
													echo '</div>';
												echo '</a>';
										echo '</div>';
									endwhile;
								echo '</div></div>';
								wp_reset_postdata();
							else :
							endif;
							if (function_exists("pagination"))
							{
							    pagination($newsAndEvents->max_num_pages);
							}
						echo '
					</div>
				</div>
			</div>';
		}

	// =======================================
	//			3. Careers
	// =======================================

		function careers()
		{
			echo '<div id="careers">
					<div class="holder of-hid pos-rel">
						<img class="image" src="'.get_template_directory_uri().'/images/home/careers/bg.jpg">
						<div class="wrapper">
							<div class="title">
								<h1 class="din-black color-p">Career <br>Opportunities</h1>
								<p class="avenir-light full t-a-j">Daytona Technologies Ltd is part of the award-winning international organisation who focuses on technology at its core and we promise an environment which will allow you to work with a robust team across jurisdictions on exciting products and utilise the latest technologies.
We are currently extending our development team in Mauritius and are therefore on the hunt for creative whizzes who can embody our core values in their personal and professional life. We are looking for someone who is an analytical
thinker, customer-focused, community-centric, inquisitive, creatively passionate and is constantly pushing the boundaries to come up with new ideas and approaches to problem-solving.
<a href="mailto:careers@daytonasystems.in"><span class="color-p">hr@daytonatec.com</span></a></p>
							</div>';
							$careers = new WP_Query(
								array(
									'post_type'			=> 'daytonacareers', 
									'post_status'		=> 'publish', 
									'orderby'			=> 'menu_order',
									'order'				=> 'asc'
								)
							);
							if ($careers->have_posts()):
								echo '<div class="openings">';
									while($careers->have_posts()):$careers->the_post();
										echo '<div class="one trans-all">
												<a title="';
													the_title();
												echo '"href="';
												the_permalink();
												echo '">
													<div class="post of-hid">
														<div class="f-left icon">';
															echo '<img alt="';
																the_title();
															echo '" src="';
																echo the_post_thumbnail_url();
															echo '"/>';
														echo '</div>
														<div class="f-left desc">
															<h4 class="din-black color-b">';
																the_title();
															echo '</h4>
															<p class="avenir-light defn">';
																echo substr(strip_tags(wp_strip_all_tags(get_the_content(), true)), 0, 98);
															echo ' ….</p>
															<div class="read-more">
																<p class="avenir-black color-p">READ MORE</p>
															</div>
														</div>
													</div>
												</a>';
										echo '</div>';
									endwhile;
								echo '</div>';
								wp_reset_postdata();
							else :
							endif;
					echo '</div>
					</div>
				</div>';
		}

	// =======================================
	//			4. Gallery
	// =======================================

		// =======================================
		//			4.1 Heading
		// =======================================

			function gallery_heading()
			{
				echo '<div id="whatWeDo">';
					echo '<div class="holder pos-rel cover-img" style="background-image: url('.get_template_directory_uri().'/images/gallery/bg.jpg);">';
						echo '<div class="v-cover">';
							echo '<div class="v-one">';
								echo '<div class="wrapper">';
									echo '<h1 class="din-black color-white">Daytona<br>Photo Gallery<br>Albums - </h1>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}

		// =======================================
		//			4.1 Albums
		// =======================================

			function gallery()
			{
				echo '<div id="albums">';
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$albums = new WP_Query(
					array(
						'post_type'			=> 'daytonagallery', 
						'post_status'		=> 'publish', 
						'posts_per_page'	=> 9, 
						'order_by'			=> 'post_date', 
						'order'				=> 'desc',
						'paged'				=> $paged
					)
				);
				if ($albums->have_posts()):
					echo '<div class="wid-1-1 of-hid">';
						while($albums->have_posts()):$albums->the_post();
							echo '<div class="wid-1-3 f-left pos-rel image trans-all cover-img" style="background-image: url(';
								echo the_post_thumbnail_url();
								echo ');"/>';
									echo '<a title="';
										the_title();
									echo ' "href="';
									the_permalink();
									echo '">';
										echo '<img class="wid-1-1 dis-b" src="'.get_template_directory_uri().'/images/gallery/4x3.png">';
										echo '<div class="overlay trans-all"></div>';
										echo '<h4 class="din-black color-white trans-all">';
											the_title();
										echo '</h4>';
									echo '</a>';
							echo '</div>';
						endwhile;
					echo '</div>';
					wp_reset_postdata();
				else :
				endif;
				if (function_exists("pagination"))
				{
				    pagination($albums->max_num_pages);
				}
				echo '</div>';
			}

	// =======================================
	//			5. Life at Daytona
	// =======================================

			function latest_gallery($limit)
			{
				echo '<div id="albums">';
					$gallery = new WP_Query(
						array(
							'post_type'			=> 'daytonagallery', 
							'post_status'		=> 'publish', 
							'order_by'			=> 'publish_date', 
							'order'				=> 'desc',
							'posts_per_page'	=> $limit
						)
					);
					if ($gallery->have_posts()):
						echo '<div class="wid-1-1 of-hid">';
							while($gallery->have_posts()):$gallery->the_post();
								echo '<div class="wid-1-3 f-left pos-rel image trans-all cover-img" style="background-image: url(';
									echo the_post_thumbnail_url();
									echo ');"/>';
										echo '<a title="';
											the_title();
										echo ' "href="';
										the_permalink();
										echo '">';
											echo '<img class="wid-1-1 dis-b" src="'.get_template_directory_uri().'/images/gallery/4x3.png">';
											echo '<div class="overlay trans-all"></div>';
											echo '<h4 class="din-black color-white trans-all">';
												the_title();
											echo '</h4>';
										echo '</a>';
								echo '</div>';
							endwhile;
						echo '</div>';
						wp_reset_postdata();
					else :
					endif;
					echo '<div class="btn-holder t-a-c">';
						echo '<div class="dis-i-b">';
							echo '<a title="View all" href="'.get_site_url().'/gallery/'.'">';
								echo '<button class="color-p avenir-black t-t-u trans-all">View all</button>';
							echo '</a>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}

	// =======================================
	//			6. What We Do
	// =======================================

		function services()
		{
			echo '<div id="services">';
				echo '<div class="holder of-hid pos-rel">';
					echo '<img class="image" src="'.get_template_directory_uri().'/images/home/careers/bg.jpg">';
					echo '<div class="wrapper">';
						echo '<div class="title">';
							echo '<h1 class="din-black color-p">About Us</h1>';
						echo '</div>';
						$services = new WP_Query(
							array(
								'post_type'			=> 'daytonawhatwedo', 
								'post_status'		=> 'publish', 
								'orderby'			=> 'menu_order', 
								'order'				=> 'asc'
							)
						);
						if ($services->have_posts()):
							echo '<div class="services">';
								echo '<div class="all">';
									while($services->have_posts()):$services->the_post();
										echo '<div class="one trans-all of-hid">';
											echo '<div class="f-left icon">';
												echo '<img alt="';
														the_title();
													echo '" src="';
														echo the_post_thumbnail_url();
													echo '"/>';
												echo '</div>';
												echo '<div class="f-left desc">';
														echo '<h4 class="din-black color-b pos-rel">';
															the_title();
														echo '</h4>';
														echo '<p class="avenir-light">';
															echo the_content();
														echo '</p>';
												echo '</div>';
										echo '</div>';
									endwhile;
								echo '</div>';
							echo '</div>';
							wp_reset_postdata();
						else :
						endif;
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}

	// =======================================
	//			7. Pagination
	// =======================================

		function pagination($pages = '', $range = 4)
		{    		
    		$showitems = ($range * 2)+1;  
			global $paged;
			if(empty($paged))
			{				
				$paged = 1;
			}
			if($pages == '')
			{
				global $wp_query;
				$pages = $wp_query->max_num_pages;
				if(!$pages){
					$pages = 1;
				}
			}   
			if(1 != $pages)
			{
         		echo '<ul class="pagination">';
         		if($paged > 2 && $paged > $range+1 && $showitems < $pages)
         		{
         			echo "<li><a href='".get_pagenum_link(1)."'>F</a></li>";
         		}
         		if($paged > 1 && $showitems < $pages)
         		{
         			echo "<li><a href='".get_pagenum_link($paged - 1)."'><</a></li>";
         		}
         		for ($i=1; $i <= $pages; $i++){
             
             		if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             		{
                 		echo ($paged == $i)? '<li class="current"><a href="#">'.$i.'</a></li>':'<li><a href="'.get_pagenum_link($i).'" >'.$i.'</a></li>';
             		}
         		}
        		if ($paged < $pages && $showitems < $pages)
        		{
         			echo "<li><a href=\"".get_pagenum_link($paged + 1)."\">></a></li>";
     			}
         		if (($paged < ($pages-1)) &&  (($paged+$range-1) < $pages) && $showitems < $pages)
         		{
         			echo "<li><a href='".get_pagenum_link($pages)."'>L</a></li>";
         		}
         		echo "</ul>";
         	}
		}

	// =======================================
	//			N. End
	// =======================================
